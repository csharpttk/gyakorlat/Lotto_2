﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto_2
{
    class Program
    {
        const int maxhét = 52;
        const int húzásszám = 5;
        const int maxérték = 90;
        static int[][] húzások;
        static int hetekszáma;
        static public void generálás()
        {
            int újhúzás, k;
            Random r = new Random();
            húzások = new int[maxhét][];
            Console.Write("Hány hét? ");
            hetekszáma = Convert.ToInt16(Console.ReadLine()); //nincs ellenőrzés
            for (int i = 0; i < hetekszáma; i++)
            {
                húzások[i] = new int[húzásszám];
                int j = 0;
                while ( j < húzásszám)
                {
                    újhúzás = r.Next(1, maxérték + 1);
                    /*
                    k = 0;
                    while (k < j && húzások[i][k] != újhúzás) k++; //ellenőrzés, volt-e már
                    if (k == j) húzások[i][j] = újhúzás;
                    */
                    if (!húzások[i].Contains(újhúzás))
                    {
                        húzások[i][j++] = újhúzás;
                    } 
                }
                /*
                for (int l = 0; l < húzásszám; l++)
                {
                    Console.Write("{0} ", húzások[i][l]);
                } 
                Console.WriteLine();
                */
                Console.WriteLine(String.Join(" ", húzások[i]));              
            }
        }
        static public void statisztika()
        {
            int[] hányszor = new int[maxérték + 1];
            for (int i = 1; i <= maxérték; i++) hányszor[i] = 0;

            for (int j = 0; j < hetekszáma; j++)
            {
                for (int k = 0; k < húzásszám; k++)
                { ++hányszor[húzások[j][k]]; }
            }

            for (int i = 1; i <= maxérték; i++)
            {
                Console.WriteLine("{0} - {1} ", i, hányszor[i]);
            }
        }
        static void Main(string[] args)
        {
            generálás();
            statisztika();

        }
    }
}
